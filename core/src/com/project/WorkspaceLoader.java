package com.project;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class WorkspaceLoader {

    Scanner scanner;
    GlyphLayout layout;
    String str;
    int nowY;
    int startX;

    private Picture loadPicture() {
        int y = this.nowY, width = 0, height = 0;
        String location = "";
        this.str = this.scanner.nextLine();
        while (!this.str.contains("{{PIC}}")) {
            if (this.str.startsWith("width=")) {
                width = Integer.parseInt(this.str.substring(6));
            }
            else if (this.str.startsWith("height=")) {
                height = Integer.parseInt(this.str.substring(7));
            }
            else if (this.str.startsWith("location=")) {
                location = this.str.substring(9);
            }
            this.str = this.scanner.nextLine();
        }
        this.nowY -= height * Gdx.graphics.getHeight() / 480 + 5;
        return new Picture(y, width, height, location);
    }

    private Link loadLink (String tag) {
        String s = tag;
        while (!s.startsWith("load=") && !s.startsWith("copy=")) {
            s = scanner.nextLine();
        }
        StringBuilder text = this.processTag(tag + this.scanner.nextLine() , tag);
        Font font = tag.contains("MAIN") ? InterfaceParameters.MAIN_FONT : InterfaceParameters.HEADER_FONT;
        return new Link (this.startX, this.nowY, font, text.toString(), s);
    }

    private StringBuilder processTag (String line, String tag) {
        Font font = null;
        int length;
        StringBuilder string = new StringBuilder();
        if (tag.equals("{{MAIN}}") || tag.equals("{{MAIN-LINK}}")) {
            font = InterfaceParameters.MAIN_FONT;
        }
        else if (tag.equals("{{HEADER}}") || tag.equals("{{HEADER-LINK}}")) {
            font = InterfaceParameters.HEADER_FONT;
        }
        else if (tag.equals("{{DEFINITION}}")) {
            font = InterfaceParameters.DEFINITION_FONT;
        }
        if (!tag.contains("LINK")) {
            line = line.replaceFirst("\\{\\{" + tag.substring(2), "      ");
        }
        else {
            line = line.replaceFirst("\\{\\{" + tag.substring(2), "");
        }
        this.layout = new GlyphLayout();
        while (scanner.hasNextLine() || line.endsWith(tag+"\n")) {
            this.layout.setText(font.font, line);
            if (line.endsWith(tag + "\n")) {
                line = line.replace(tag + "\n", "");
                this.layout.setText(font.font, line);
                while (this.layout.width > Gdx.graphics.getWidth() - this.startX) {
                    length = (int) ((this.layout.width - (Gdx.graphics.getWidth() - this.startX)) / font.layout.width);
                    int i = 0;
                    this.layout.setText(font.font, line.substring(0, line.length() - length + i));
                    if (this.layout.width > Gdx.graphics.getWidth() - this.startX) {
                        while (this.layout.width > Gdx.graphics.getWidth() - this.startX) {
                            --i;
                            this.layout.setText(font.font, line.substring(0, line.length() - length + i));
                        }
                    }
                    else if (this.layout.width < Gdx.graphics.getWidth() - this.startX) {
                        while (this.layout.width < Gdx.graphics.getWidth() - this.startX) {
                            ++i;
                            this.layout.setText(font.font, line.substring(0, line.length() - length + i));
                        }
                    }
                    string.append(line, 0, line.length() - length + i).append("\n");
                    if (line.charAt(line.length() - length + i) == ' ') {
                        i++;
                    }
                    line = line.substring(line.length() - length + i);
                    this.layout.setText(font.font, line);
                }
                string.append(line);
                break;
            }
            while (this.layout.width > Gdx.graphics.getWidth() - this.startX) {
                length = (int) ((this.layout.width - (Gdx.graphics.getWidth() - this.startX)) / font.layout.width);
                int i = 0;
                this.layout.setText(font.font, line.substring(0, line.length() - length + i));
                if (this.layout.width > Gdx.graphics.getWidth() - this.startX) {
                    while (this.layout.width > Gdx.graphics.getWidth() - this.startX) {
                        --i;
                        this.layout.setText(font.font, line.substring(0, line.length() - length + i));
                    }
                }
                else if (this.layout.width < Gdx.graphics.getWidth() - this.startX) {
                    while (this.layout.width < Gdx.graphics.getWidth() - this.startX) {
                        ++i;
                        this.layout.setText(font.font, line.substring(0, line.length() - length + i));
                    }
                }
                string.append(line, 0, line.length() - length + i).append("\n");
                if (line.charAt(line.length() - length + i) == ' ') {
                    i++;
                }
                line = line.substring(line.length() - length + i);
                this.layout.setText(font.font, line);
            }
            if (!line.endsWith(tag + "\n")) {
                string.append(line);
            }
            if (scanner.hasNextLine()) {
                line = scanner.nextLine() + "\n";
            }
        }
        this.layout.setText(font.font, string.toString());
        this.nowY -= this.layout.height + 15;
        return string;
    }

    public void contentLoad (String location, int startX, int startY, Workspace workspace) {
        this.nowY = startY;
        this.startX = startX;
        int picture_count = 0;
        boolean load_picture = (workspace.pictures == null) || workspace.pictures.isEmpty();
        InterfaceParameters.HEADER_FONT.layout.setText(InterfaceParameters.HEADER_FONT.font, "W");
        InterfaceParameters.MAIN_FONT.layout.setText(InterfaceParameters.MAIN_FONT.font, "W");
        InterfaceParameters.DEFINITION_FONT.layout.setText(InterfaceParameters.DEFINITION_FONT.font, "W");
        try {
            this.scanner = new Scanner(new InputStreamReader(new FileInputStream(location), StandardCharsets.UTF_8));
            while (scanner.hasNextLine()) {
                this.str = this.scanner.nextLine() + "\n";
                if (this.str.contains("{{MAIN}}")) {
                    workspace.addItem(new SimpleText(this.startX, nowY, InterfaceParameters.MAIN_FONT,
                                      processTag(this.str, "{{MAIN}}").toString()));
                }
                else if (this.str.contains("{{HEADER}}")) {
                    workspace.addItem(new SimpleText(this.startX, nowY, InterfaceParameters.HEADER_FONT,
                                      processTag(this.str, "{{HEADER}}").toString()));
                }
                else if (this.str.contains("{{HEADER-LINK}}")) {
                    workspace.addItem(loadLink("{{HEADER-LINK}}"));
                }
                else if (this.str.contains("{{MAIN-LINK}}")) {
                    workspace.addItem(loadLink("{{MAIN-LINK}}"));
                }
                else if (this.str.contains("{{DEFINITION}}")) {
                    workspace.addItem(new SimpleText(this.startX, nowY, InterfaceParameters.DEFINITION_FONT,
                            processTag(this.str, "{{DEFINITION}}").toString()));
                }
                else if (this.str.contains("{{PIC}}")) {
                    this.nowY -= 5;
                    if (load_picture) {
                        workspace.addItem(loadPicture());
                    }
                    else if (workspace.pictures.size() - 1 >= picture_count){
                        workspace.pictures.get(picture_count).maxY = this.nowY;
                        this.nowY -= workspace.pictures.get(picture_count).height + 5;
                        picture_count++;
                        while (!this.str.contains("{{PIC}}")) {
                            this.str = this.scanner.nextLine();
                        }
                    }
                }
            }
            InterfaceParameters.HEADER_FONT.layout.setText(InterfaceParameters.HEADER_FONT.font, " ");
            InterfaceParameters.MAIN_FONT.layout.setText(InterfaceParameters.MAIN_FONT.font, " ");
            InterfaceParameters.DEFINITION_FONT.layout.setText(InterfaceParameters.DEFINITION_FONT.font, " ");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            this.scanner.close();
        }
    }

}